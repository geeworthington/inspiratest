function testCount() {

    for (let count = 1; count <= 100; count++) {
        let $sense = count % 3 === 0,
            $making = count % 5 === 0;
        document.write($sense ? $making ? " <span class='special-value'>Inspira Digital</span> " : " <span>Digital</span> " : $making ? " <span>Inspira</span> " : '<span>'+count+'</span>');
    }

}



